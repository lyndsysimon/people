FROM python:3.8.6-alpine

#RUN apk update && \
#  apk add \
#    build-base \
#    postgresql \
#    postgresql-dev \
#    libpq \
#    bind-tools

RUN pip install pipenv

RUN mkdir /app
WORKDIR /app
COPY Pipfile .
COPY Pipfile.lock .
RUN pipenv install --dev
ENV FLASK_ENV=development \
    FLASK_APP=api:get_app 

COPY . .

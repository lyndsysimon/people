# People API

## Dockerized Usage

Assuming you have docker and docker-compose installed, this application may be
run locally in containers:

To run the tests:

    docker-compose run test

To run the web server for local testing/verification:

    docker-compose run -p 5000:5000 web

Note that the docker images must be rebuild to reflect changes to the source
directory.

Also, it may be useful to start the MongoDB service and leave it running in the
background, so it need not be restarted each invocation:

    docker-compose up mongo

## Base Commands

All commands should be run with an active pipenv environment: `pipenv shell`

To run the development server:

    FLASK_ENV=development FLASK_APP=app:get_app flask run

To run the linter:

    pycodestyle app

To run the tests:

    python -m pytest

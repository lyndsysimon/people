from flask import jsonify
from flask import make_response
from flask import request
from flask import url_for
from flask.views import MethodView
from flask_pymongo import ObjectId

from api import mongo
from api.schemas import PersonRecordSchema
from api.schemas import PersonSchema

class PersonAPI(MethodView):
    def get(self, person_id=None, version=None):
        """Get one or more existing Person instances"""
        if person_id is None:
            return jsonify([
                p['versions'][-1]
                for p
                in mongo.db.people.find({'deleted': False})
            ])
        record = mongo.db.people.find_one_or_404(ObjectId(person_id))

        if record['deleted']:
            return '', 410

        if version is None:
            return record['versions'][-1]

        try:
            return record['versions'][version]
        except IndexError:
            return '', 404

    def post(self):
        """Create a new Person instance and return the URI"""
        version = PersonSchema().load(request.get_json())
        record = PersonRecordSchema().load({'versions': [version, ]})
        new_person_id = mongo.db.people.insert_one(record)

        response = make_response("", 201)
        response.headers['Location'] = url_for(
            'person_api',
            person_id=new_person_id,
        )
        return response

    def put(self, person_id):
        """Update an existing Person instance"""
        selector = {'_id': ObjectId(person_id)}
        json = request.get_json()
        version = PersonSchema().load(json)
        record = mongo.db.people.find_one_or_404(selector)
        record['versions'].append(version)
        # TODO: This introduces a race condition
        mongo.db.people.replace_one(selector, record)
        return '', 200


    def delete(self, person_id):
        """Logically delete a Person instance"""
        selector = {'_id': ObjectId(person_id)}
        record = mongo.db.people.find_one_or_404(selector)
        record['deleted'] = True
        # TODO: This introduces a race condition
        mongo.db.people.replace_one(selector, record)
        return '', 200

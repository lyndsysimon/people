from marshmallow import Schema, fields


class PersonSchema(Schema):
    first_name = fields.Str()
    middle_name = fields.Str(allow_none=True)
    last_name = fields.Str()
    age = fields.Int()
    email = fields.Email()


class PersonRecordSchema(Schema):
    _id = fields.Str(allow_none=True)
    versions = fields.List(fields.Nested(PersonSchema()))
    deleted = fields.Boolean(default=False)

import os

from flask import Flask
from flask_pymongo import PyMongo


mongo = PyMongo()


def get_app(env=None):
    """Return a configured Flask application"""
    app = Flask(__name__)

    if env:
        app.env = env

    _config_settings(app)
    _config_extensions(app)
    _config_routing(app)

    return app


def _config_settings(app):
    """Configure app settings"""
    app.config['SECRET_KEY'] = 'This is a terrible secret'
    app.config['MONGO_URI'] = os.getenv(
        'MONGO_URI',
        'mongodb://localhost:27017/peopleAPI',
    )

def _config_extensions(app):
    """Configure Flask extensions"""
    mongo.init_app(app)


def _config_routing(app):
    """Wire up views"""
    from .views import PersonAPI
    person_view = PersonAPI.as_view('person_api')

    app.add_url_rule( '/person/',
        view_func=person_view,
        methods=['GET', 'POST'],
    )
    app.add_url_rule(
        '/person/<string:person_id>/',
        view_func=person_view,
        methods=['GET', 'PUT', 'DELETE'],
    )
    app.add_url_rule(
        '/person/<string:person_id>/<int:version>/',
        view_func=person_view,
        methods=['GET', ],
    )

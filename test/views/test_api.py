import pytest

from api import mongo
from api.schemas import PersonRecordSchema
from api.schemas import PersonSchema


class TestPersonAPI:

    @pytest.fixture
    def person1(self, app):
        with app.app_context():
            return mongo.db.people.insert_one({
                'versions': [
                    {
                        'first_name': 'Lyndsy',
                        'middle_name': None,
                        'last_name': 'Simon',
                        'email': 'lyndsy@lyndsysimon.com',
                        'age': 36,
                    },
                ],
                'deleted': False,
            }).inserted_id

    @pytest.fixture
    def person2(self, app):
        with app.app_context():
            return mongo.db.people.insert_one({
                'versions': [
                    {
                        'first_name': 'Tamsyn',
                        'middle_name': 'Janelle',
                        'last_name': 'Simon',
                        'email': 'lyndsy@lyndsysimon.com',
                        'age': 36,
                    },
                ],
                'deleted': False,
            }).inserted_id

    def test_create(self, app, client):
        with app.app_context():
            assert mongo.db.people.count_documents({}) == 0

        response = client.post('/person/', json={
            'first_name': 'Lyndsy',
            'middle_name': None,
            'last_name': 'Simon',
            'email': 'lyndsy@lyndsysimon.com',
            'age': 36,
        })
        assert response.status_code == 201
        assert 'Location' in response.headers
        with app.app_context():
            assert mongo.db.people.count_documents({}) == 1

    def test_get_empty(self, client):
        response = client.get('/person/')
        assert response.status_code == 200
        assert response.get_json() == []

    def test_get_all(self, app, client, person1, person2):
        response = client.get('/person/')
        with app.app_context():
            assert mongo.db.people.count_documents({}) == 2

    def test_get_all_excludes_deleted(self, app, client, person1, person2):
        with app.app_context():
            mongo.db.people.update_one(
                {'_id': person2},
                { '$set': {'deleted': True,}},
            )
        response = client.get('/person/')
        assert response.status_code == 200
        assert len(response.get_json()) == 1

    def test_get_one_deleted(self, app, client, person1):
        with app.app_context():
            mongo.db.people.update_one(
                {'_id': person1},
                { '$set': {'deleted': True,}},
            )
        response = client.get(f'/person/{person1}/')
        assert response.status_code == 410
        assert response.get_json() is None

    def test_update(self, app, client, person1):
        selector = {'_id': person1}
        with app.app_context():
            json = mongo.db.people.find_one(selector)['versions'][-1]
        json['email'] = 'simon.lyndsy@gmail.com'
        response = client.put(f'/person/{person1}/', json=json)
        assert response.status_code == 200
        with app.app_context():
            new_json = mongo.db.people.find_one(selector)
        assert len(new_json['versions']) == 2

    def test_delete(self, app, client, person1):
        selector = {'_id': person1}
        response = client.delete(f'/person/{person1}/')
        assert response.status_code == 200
        with app.app_context():
            assert mongo.db.people.find_one(selector)['deleted'] == True

    def test_get_version(self, app, client, person1):
        with app.app_context():
            person = mongo.db.people.find_one({'_id': person1})
        old_version = person['versions'][0]
        with app.app_context():
            mongo.db.people.update_one(
                {'_id': person1},
                { '$set': {'versions': [
                    old_version,
                    {**old_version, **{'email': 'simon.lyndsy@gmail.com'}},
                ]}},
            )

        response = client.get(f'/person/{person1}/0/')
        assert response.status_code == 200
        assert response.get_json()['email'] == 'lyndsy@lyndsysimon.com'

        response = client.get(f'/person/{person1}/1/')
        assert response.status_code == 200
        assert response.get_json()['email'] == 'simon.lyndsy@gmail.com'

        response = client.get(f'/person/{person1}/9999/')
        assert response.status_code == 404

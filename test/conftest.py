import pytest

from api import get_app
from api import mongo

@pytest.fixture
def app():
    app = get_app(env="testing")
    with app.app_context():
        mongo.db.people.drop()
    yield app

@pytest.fixture
def client(app):
    with app.test_client() as client:
        yield client
